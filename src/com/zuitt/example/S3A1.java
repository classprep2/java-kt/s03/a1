package com.zuitt.example;

import java.util.Scanner;

public class S3A1 {
//    Creating a factorial method
    public static void main (String[] args){
        Scanner myObj = new Scanner(System.in);


        System.out.print("Input an integer whose factorial will be computed: ");
        int num = myObj.nextInt();
        int factorial = 1;
        int temp = num;
        while(temp!=0){
            factorial = factorial * temp;
            temp = temp -1;
        }

        System.out.println("The factorial of "+num+" is "+factorial+".");


    }
}
